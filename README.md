## Jenkins shell script

1. In order to run runway in Jenkins server you have to to run the following commands in your shell script

    ```
    #!/bin/bash -l
    echo "Starting creating resources"
    python3.7 -m pip install virtualenv
    python3.7 -m venv env
    source env/bin/activate


    python3.7 -m pip install runway

    export DEPLOY_ENVIRONMENT='dev'
    runway deploy
    ```

2. I installed Bitbucket Push and Pull Request Plugin in jenkins in order to automate the build process.    